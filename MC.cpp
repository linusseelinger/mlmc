
#include <iostream>
#include "Sandwich.hpp"
#include <random>


#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/DummyKernel.h"
#include "MUQ/SamplingAlgorithms/MHProposal.h"
#include "MUQ/SamplingAlgorithms/MHKernel.h"
#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SingleChainMCMC.h"
#include "MUQ/SamplingAlgorithms/MCMCFactory.h"

#include <boost/property_tree/ptree.hpp>

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;

#include "MCSampleProposal.h"
#include "UQProblem.h"


int main(int argc, const char * argv[]) {

    pt::ptree pt;
    pt.put("NumSamples", 1e2);
    pt.put("BurnIn", 1);
    pt.put("PrintLevel",3);

    auto problem = std::make_shared<MySamplingProblem>(std::make_shared<MultiIndex>(1,0));


    // Set up a Gaussian
    Eigen::VectorXd mu_prop = Eigen::VectorXd::Zero(121);
    Eigen::MatrixXd cov_prop = Eigen::MatrixXd::Ones(121,121);
    auto proposalDensity = std::make_shared<Gaussian>(mu_prop, cov_prop);

    // Let's use a proposal that only samples from the given density
    auto proposal = std::make_shared<MCSampleProposal>(pt, problem, proposalDensity);

    // Get a kernel that accepts every proposal
    std::vector<std::shared_ptr<TransitionKernel> > kernels = {std::make_shared<DummyKernel>(pt, problem, proposal)};

    auto mcmc = std::make_shared<SingleChainMCMC>(pt, kernels);


    Eigen::VectorXd startPt = Eigen::VectorXd::Zero(121);
    mcmc->Run(startPt);


    Eigen::VectorXd qoiMean = mcmc->GetQOIs()->Mean();
    std::cout << "\nQOI Mean = \n" << qoiMean.transpose() << std::endl;


    mcmc->GetSamples()->WriteToFile("MC_samples.h5", "/samples/");
    mcmc->GetQOIs()->WriteToFile("MC_samples.h5", "/qois/");

    return 0;
}

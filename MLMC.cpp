#include "MLMC.h"

namespace muq {
    namespace SamplingAlgorithms {

        // Constructor definition
        MLMC::MLMC(pt::ptree pt, std::shared_ptr<MIComponentFactory> componentFactory,
                std::shared_ptr<CostModel> costModel) :
            SamplingAlgorithm(std::shared_ptr<SampleCollection>(), std::shared_ptr<SampleCollection>()),
            componentFactory(componentFactory),
            maxNumLevels(pt.get("maxNumLevels", componentFactory->FinestIndex()->GetValue(0) + 1)),
            minNumLevels(pt.get("minNumLevels", 2)),
            costModel(costModel),
            useGilesBiasEstimate(pt.get("useGilesBiasEstimate", false)),
            isLevelAdaptive(pt.get("levelAdaptive", true)),
            useRegression(pt.get("useRegression", true)),
            maxNumWarmUpSamples(pt.get("maxNumWarmUpSamples", 10)),
            theta(pt.get("theta", 0.5)),
            useVariableMSESplitting(pt.get("useVariableMSESplitting", true)),
            theta_min(pt.get("theta_min", 0.5)),
            theta_max(pt.get("theta_max", 1)),
            continuate(pt.get("continuate", true)),
            numTols(pt.get("numTols", 10)),
            tolMulFactor(pt.get("tolMulFactor", 1.5)),
            numSamplesFraction(pt.get("numSamplesFraction", 0.01)),
            targetTol(pt.get("targetTol", 1e-3)),
            name(pt.get("name", "UntitledEstimator")),
            isVerbose(pt.get("verbose", true)) {
                for (int level = 0; level < maxNumLevels; ++level) { // Loop over all levels
                    // Add an `MIMCMCBox` to boxes
                    auto idx = std::make_shared<MultiIndex>(1, level);
                    auto box = std::make_shared<MIMCMCBox>(componentFactory, idx);
                    boxes.push_back(box);

                    // Add an `MLMCBoxStat` to stats
                    auto stat = std::make_shared<MLMCBoxStats>();
                    stat->level = std::make_shared<MultiIndex>(1);
                    stat->level->SetValue(0, level); // Set the level of the box to the given level
                    stat->N = 0; // Set the number of samples to 0
                    stats.push_back(stat);

                    // Update number of warm-up samples
                    numWarmUpSamples.push_back(pt.get("numWarmUpSamples" + std::to_string(level), 
                                pt.get("numWarmUpSamples", 10 + 1))); //TODO +1 is necessary because the first
                                                                      // call to box.Sample() doesn't do anything
                }
            }

        // This is the main function that contains the MLMC algorithm
        // TODO: what is x0 ???
        std::shared_ptr<SampleCollection> MLMC::RunImpl(std::vector<Eigen::VectorXd> const& x0) {
            targetTol = std::pow(tolMulFactor, numTols) * targetTol;
            for (int p = 0; p < numTols; ++p) {
                targetTol /= tolMulFactor;
                MLMCRun();
            }
            return nullptr;
        }

        // A single MLMC run
        std::shared_ptr<SampleCollection> MLMC::MLMCRun() {
            std::vector<int> additionalNumSamples(maxNumLevels); // A vector to store the number of samples 
                                                                 // to take on each level

            if (isVerbose)
                std::cout << PrintHeader();

            // Loop over all levels L = 0, 1, ..., maxNumLevels
            for (int L = 0; L < maxNumLevels; L++) {
                if (isVerbose)
                    std::cout << PrintLevelHeader(L);

                // Determine the number of warm-up samples and set the vector `additionalNumSamples` to contain 
                // only the number of warm-up samples on level `L` and 0 otherwise.
                std::fill(additionalNumSamples.begin(), additionalNumSamples.end(), 0);
                if (stats[L]->N == 0) { // If no samples are available yet
                    if (L > 2 && useRegression) {
                        // Regression of the cost model
                        if (typeid(*costModel) == typeid(EmptyCostModel)) // use regression
                            stats[L]->W = stats[L-1]->W * (std::pow(2, gamma) - 1);
                        else // use the cost model
                            stats[L]->W = costModel->evaluate(stats[L]->level);
                        // Regression of the variance
                        stats[L]->dV = stats[L-1]->dV / std::pow(2, beta);
                        double sum = ComputeOptimalNumSamplesSum(L);
                        int N = ComputeOptimalNumSamples(L, sum); // Evaluate optimal number of samples
                        additionalNumSamples[L] = std::min(std::max(3, N), maxNumWarmUpSamples); // TODO: 3 -> 2
                    } else {
                        additionalNumSamples[L] = numWarmUpSamples[L];
                    }
                }

                // Take warm-up samples on level L
                AddSamples(additionalNumSamples);

                // Update MLMC comlexity rates
                ComputeRates(L);

                if (isVerbose) {
                    std::cout << PrintBoxStats(L);
                    if (L > 1)
                        std::cout << PrintRates();
                }

                bool converged = false;
                while (!converged) {
                    // Compute the value of the mean square error splitting parameter (theta)
                    if (L > 1 && useVariableMSESplitting) { // TODO: beware of continuation!!!
                        ComputeSplittingTheta();
                    }

                    // Compute the optimal number of samples
                    double sum = ComputeOptimalNumSamplesSum(L);
                    double sum_f = 0;
                    for (int level = 0; level <= L; level++) {
                        additionalNumSamples[level] = ComputeOptimalNumSamples(level, sum) - stats[level]->N;
                        sum_f += std::max(0., additionalNumSamples[level] - numSamplesFraction * stats[level]->N);
                    }

                    // Take additional samples
                    if (sum_f == 0) { // Or a bound on the variance of the estimator ( < theta * targetTol^2 )
                        if (isVerbose)
                            std::cout << PrintStatisticalErrorConvergence();
                        converged = true;
                    } else {
                        if (isVerbose) {
                            std::cout << PrintNoStatisticalErrorConvergence();
                            std::cout << "The optimal number of samples at each level is:" << std::endl;
                            std::cout << PrintOptimalNumSamples(additionalNumSamples, L);
                        }
                        AddSamples(additionalNumSamples);
                    }
                }

                // Print status
                if (isVerbose)
                    std::cout << PrintBoxStats(L);

                if (L > 1) {
                    // Update MLMC complexity rates
                    ComputeRates(L);

                    // Print rates at the bottom of the table
                    if (isVerbose)
                        std::cout << PrintRates();

                    // Compute variance of the estimator
                    ComputeVarianceOfEstimator();

                    // Compute bias
                    ComputeBias(1, L);

                    // Print statistics about the estimator
                    if (isVerbose)
                        std::cout << PrintStatistics(L);

                    // Decide if we should add an extra level
                    if (isLevelAdaptive) {
                        if (L < minNumLevels - 1) { // Minimum levels is not reached yet
                            if (isVerbose)
                                std::cout << PrintMinLevelsNotReached();
                        } else {
                            double rmse = std::sqrt(varest + std::pow(bias, 2));
                            if (rmse < targetTol) { // Bias convergence reached
                                if (isVerbose)
                                    std::cout << PrintBiasConvergence();
                                break;
                            } else if (isVerbose) {
                                if (L < maxNumLevels - 1) // No bias convergence reached, adding an extra level
                                    std::cout << PrintNoBiasConvergence();
                                else // No bias convergence reached, but no more levels to add!
                                    std::cout << PrintNoBiasConvergenceNoMoreLevels();
                            }
                        }
                    }
                }
            }

            if(isVerbose)
                std::cout << PrintFooter();

            return nullptr;
        }

    }
}

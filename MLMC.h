/*!
 * \file MLMC.h
 * \brief A header file for the implementation of Multilevel Monte Carlo (MLMC) in MUQ.
 *
 * MLMC is a special case of the MLMCMC algorithm already implemented in MUQ.
 */

// +-------------------------------------------------------------------------------+
// |                                 MLMC WISHLIST                                 |
// +--------+----------------------------------------------------------------------+
// | STATUS |                        DESCRIPTION                                   |
// +--------+----------------------------------------------------------------------+
// |        |                                                                      |
// |   V    | Link with MUQ :-)                                                    |
// |        |                                                                      |
// |   V    | Add fancy print statements that show level, E0, dE, V0, dV, W and    |
// |        | and N after every outer iteration.                                   |
// |        |                                                                      |
// |   V    | Bias estimation through log-linear regression of dE (mean of         | 
// |        | multilevel differences) - currently we use all levels...             |
// |        |                                                                      |
// |   V    | Add calculation of the rates in the MLMC convergence theorem and     |
// |        | output to the user. This is also known as weak/strong convergence    |
// |        | checks.                                                              |
// |        |                                                                      |
// |   X    | Input checking. Handle unknown keys in pt.                           |
// |        |                                                                      |
// |   V    | Add possibility to use a user-provided cost model, which is probably |
// |        | more accurate than the current approach (using wall clock times).    |
// |        | There might be issues in the parallel sampling, and for actual       |
// |        | engineering problems, the measured wall clock time might vary        |
// |        | a lot from sample to sample.                                         |
// |        | TODO: integrate with pt                                              |
// |        |                                                                      |
// |   V    | Add kw to choose regression for bias estimation using either         |
// |        | all levels (more robust) or using only the last two levels when      |
// |        | L > 1 (as in Giles' original SDE paper). Choose different models.    |
// |        |                                                                      |
// |   V    | Add kw to choose number of warm-up samples                           |
// |        | (`nb_of_warm_up_samples`), potentially different on each level       |
// |        |                                                                      |
// |   V    | Add support to avoid the warm-up samples on higher levels, but       |
// |        | instead directly estimate the variance and cost, and, hence, the     |
// |        | required number of samples. Then take this amount of samples, capped |
// |        | by a maximum number of warm-up samples to correct for                |
// |        | over-estimation.                                                     |
// |        |                                                                      |
// |   V    | Add support for variable mean-square error (MSE) splitting parameter |
// |        | `theta` (the key point in "continuation" MLMC).                      |
// |        |                                                                      |
// |   V    | Check for convergence of statistical error after each "optimal       |
// |        | number of samples" loop.                                             |
// |        |                                                                      |
// |   V    | Add a toggle kw for variable mean-square error (MSE)                 |
// |        | splitting (because sometimes, this is not desired).                  |
// |        |                                                                      |
// |   V    | Add kw's to specify min and max error splitting parameters.          |
// |        |                                                                      |
// |   V    | Add support for continuation: run the same estimator for a sequence  |
// |        | of larger tolerances to get better estimates for the bias and the    |
// |        | MSE splitting parameter `theta` (if enabled).                        |
// |        |                                                                      |
// |   V    | Add support for biased computation when maximum number of levels is  |
// |        | exceeded.                                                            |
// |        |                                                                      |
// |   V    | Add a toggle kw to activate continuation, and add kw's to choose the |
// |        | continuation tolerances (or number of tolerances and multiplication  |
// |        | factor).                                                             |
// |        |                                                                      |
// |   V    | Add a kw for minimum number of levels.                               |
// |        |                                                                      |
// |   X    | Add support for saving/loading an MLMC simulation, such that         |
// |        | previous simulation results for a larger tolerance are not lost.     |
// |        | This requires also that the simulation can be given a name and dir   |
// |        | where to store the results (depends on support already in MUQ?)      |
// |        |                                                                      |
// |   X    | Add functionality aka "Reporter.jl": automatic generation of         | 
// |        | summary report in html/pdf with plots of MLMC statistics, number     |
// |        | of samples on each level, cost complexity plot etc. This is best     |
// |        | handled with a Python wrapper?                                       |
// |        |                                                                      |
// |   V    | Add "verbose" or "silent" mode.                                      |
// |        |                                                                      |
// |   X    | Add support for multiple quantities of interest. The `mlmc_stats_t`  |
// |        | should then be adapted such that it can handle multiple quantities,  |
// |        | and the variance and bias in the MLMC algorithm should be based on   |
// |        | the quantity of interest with the largest root mean square error.    |
// |        | Add a kw that contains the number of quantities of interest.         |
// |        | Add a kw for `qoi_with_max_error.                                    |
// |        |                                                                      |
// |   X    | How to handle massive parallel sampling? Small updates of the        |
// |        | required number of samples might not be efficient...                 |
// |        |                                                                      |
// |   X    | Think aboout adding other methods, such as MIMC, MLQMC, MIQMC, AMIMC |
// |        | AMIQMC, UMLMC, UMIMC, UMLQMC, UMIMC...                               |
// |        |                                                                      |
// +--------+----------------------------------------------------------------------+

#ifndef MLMC_H
#define MLMC_H

#include <boost/property_tree/ptree.hpp>
#include <cmath>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <numeric>
#include <string>
#include <ctime>
#include <limits>

#include "MUQ/SamplingAlgorithms/MIMCMCBox.h"
#include "MUQ/SamplingAlgorithms/MIComponentFactory.h"
#include "MUQ/SamplingAlgorithms/SamplingAlgorithm.h"

namespace muq {
    namespace SamplingAlgorithms {

        /*!
         * \brief A structure to represent some statistics of an `MIMCMCBox`.
         *
         * The intent of this structure is to conveniently gather the statistics of the
         * `MIMCMCBox`, such that it can be used by the MLMC algorithm, without 
         * forcing a recomputation of the statistics of the box at every occurance.
         *
         * \warn This data structure should not be used directly by the user.
         */
        struct MLMCBoxStats {
            std::shared_ptr<MultiIndex> level; // the value of the level to which this `MLMCBoxStats` belongs
            double E0; // average of the samples at this level
            double dE; // average of the multilevel differences of the samples at this level
            double V0; // sample variance of the samples at this level
            double dV; // sample variance of the multilevel differences of the samples at this level
            double W;  // cost per sample
            double elapsed; // total elapsed time at this level (used to measure the cost in real time)
            int N;     // number of samples at this level
        };

        /*!
         * \brief Returns a string representation of an `MLMCBoxStats`.
         *
         * \param buff The stream to add to
         * \param stat The `MLMCBoxStats` to display
         * \return The stream with the `MLMCBoxStats` added
         */
        std::ostream& operator<<(std::ostream& buff, const MLMCBoxStats& stat);

        /*!
         * \brief An abstract class to represent a cost model.
         */
        class CostModel
        {
            public:
                virtual double evaluate(std::shared_ptr<MultiIndex> const& index) = 0;
        };

        /*!
         * A class used to represent an empty cost model
         */
        class EmptyCostModel : public CostModel {

            public:
                /*!
                 * \brief Returns an instance of the `GeometricCostModel` class.
                 */
                EmptyCostModel();

                /*!
                 * \brief Evaluate the geometric cost model at the given level.
                 */
                double evaluate(std::shared_ptr<MultiIndex> const& index);
        };


        /*!
         * A class used to represent a geometric cost model
         *
         * The cost model has the form C(l) = constant * base ^ (exponent * l).
         */
        class GeometricCostModel : public CostModel {
            double constant;
            double base;
            double exponent;

            public:

                /*!
                 * \brief Returns an instance of the `GeometricCostModel` class.
                 *
                 * \param constant double The constant in the cost model
                 * \param base double The base in the cost model
                 * \param exponent double The exponent in the cost model
                 * \return model An instance of the `GeometricCostModel` class with the given parameters
                 */
                GeometricCostModel(double constant, double base, double exponent);

                /*!
                 * \brief Evaluate the geometric cost model at the given level.
                 *
                 * \param index An `std::shared_ptr` to a `MultiIndex` where the cost model must be evaluated.
                 * \return cost double The geometric cost model evaluated at the given level.
                 */
                double evaluate(std::shared_ptr<MultiIndex> const& index);
        };

        /*!
         *\brief A cost model factory
         */
        std::shared_ptr<CostModel> CostModelFactory(CostModel const& costModel);

        /*!
         * \brief A class used to represent the MLMC algorithm.
         */
        class MLMC : public SamplingAlgorithm {
            public:
                /*!
                 * \brief Returns an instance of the `MLMC` class.
                 *
                 * \param pt A `boost::property_tree::ptree` that contains the settings for the MLMC algorithm
                 * \param componentFactory A `shared pointer` to an `MIComponentFactory`
                 * \param costModel A `shared pointer` to a `CostModel`
                 * \return mlmc An instance of the `MLMC` class with the given settings, for the model problem
                 * specified by the given component factory.
                 */
                MLMC(boost::property_tree::ptree pt, std::shared_ptr<MIComponentFactory> componentFactory, std::shared_ptr<CostModel> costModel);

                // Override from SamplingAlgorithm
                // TODO: This is currently not used, returns a nullptr
                virtual std::shared_ptr<SampleCollection> GetSamples() const override;
                
                // Override from SamplingAlgorithm
                // TODO: This is currently not used, returns a nullptr
                virtual std::shared_ptr<SampleCollection> GetQOIs() const override;

                /*!
                 * \brief Returns the `MIMCMCBox` at the given level.
                 *
                 * \param level The level for which the `MIMCMCBox` should be returned
                 * \return box The `MIMCMCBox` at the given level
                 *
                 * \throw out_of_range If no `MLMCBox` exists at the given level
                 */
                std::shared_ptr<MIMCMCBox> GetBox(int level);
                
                /*!
                 * \brief Returns the `MIMCMCBoxStat` at the given level.
                 *
                 * \param level The level for which the `MIMCMCBoxStat` should be returned
                 * \return box The `MIMCMCBoxStat` at the given level
                 *
                 * \throw out_of_range If no `MLMCBoxStats` exists at the given level
                 */
                std::shared_ptr<MLMCBoxStats> GetBoxStats(int level);
                
                /*!
                 * \brief Returns a string representation of the statistics of the `MLMCBoxStats` at each level
                 * in table format.
                 *
                 * Calls `PrintBoxStats` with `maxNumLevels`.
                 *
                 * \return str The string representation.
                 */
                std::string PrintBoxStats();

                /*!
                 * \brief Returns a string representation of the statistics of the `MLMCBoxStats` at each level
                 * up to the given level in table format.
                 *
                 * \param numLevels The maximum level to print. 
                 *
                 * \return str The string representation.
                 */
                std::string PrintBoxStats(int numLevels);

            protected:
                /*!
                 * \brief Runs the MLMC algorithm.
                 */
                // Override from SamplingAlgorithm (Run() calls RunImpl() on line 19 of SamplingAlgorithm.cpp)
                // TODO: Not sure what the input argument x0 is, though...
                virtual std::shared_ptr<SampleCollection> RunImpl(std::vector<Eigen::VectorXd> const& x) override;

            private:
                // A `shared_ptr` to the `MIComponentFactory`
                std::shared_ptr<MIComponentFactory> componentFactory;
                // A `vector` of `shared_ptr`s to an `MIMCMCBox` on each level
                std::vector<std::shared_ptr<MIMCMCBox>> boxes;
                // A `vector` of `shared_ptr`s to an `MIMCMCBoxStat` on each level
                std::vector<std::shared_ptr<MLMCBoxStats>> stats;
                // The maximum number of levels of this MLMC method
                const int maxNumLevels;
                // The minimum number of levels of this MLMC method
                const int minNumLevels;
                // The bias of this MLMC estimator
                double bias = std::numeric_limits<double>::infinity();
                // The variance of this MLMC estimator
                double varest = std::numeric_limits<double>::infinity();
                // The constant in the fit through the expected value 
                double a = 0;
                // The MLMC complexity rate alpha
                double alpha = 0;
                // The MLMC complexity rate beta
                double beta = 0;
                // The MLMC complexity rate gamma
                double gamma = 0;
                // A shared pointer to a cost model
                std::shared_ptr<CostModel> costModel;
                // A bool to decide wether Giles' bias estimate should be used
                const bool useGilesBiasEstimate;
                // A bool to decide if the algorithm is level-adaptive
                const bool isLevelAdaptive;
                // A bool to decide if we should use regression for the number of warm-up samples
                const bool useRegression;
                // The maximum number of warm-up samples in the regression
                const int maxNumWarmUpSamples;
                // The number of warm-up samples on each level (default is 10)
                std::vector<int> numWarmUpSamples;
                // The MSE splitting parameter
                double theta;
                // A bool to decide of we should use variable mean square error splitting (variable theta)
                const bool useVariableMSESplitting;
                // The minimum value of theta in case variable mean square error splitting is used
                double theta_min;
                // The maximum value of theta in case variable mean square error splitting is used
                double theta_max;
                // A bool to decide if we should use continuation
                const bool continuate; // TODO: add option to let user specify the requested tolerances
                                       // Can we pass this vector to the boost::ptree?
                // The number of continuation tolerances
                const int numTols;
                // The continuation tolerance multiplication factor
                const double tolMulFactor;
                // The fraction of the number of samples to take into account to check for convergence of the
                // statistical error
                const double numSamplesFraction;
                // The target tolerance on the root mean square error (RMSE) of the QOI (default is 1e-3)
                double targetTol;
                // An `std::string` with the name of this MLMC estimator
                std::string name;
                // A bool to decide if we should print progress to the output stream (default is true)
                const bool isVerbose;

                // A single MLMC run
                std::shared_ptr<SampleCollection> MLMCRun();
                
                /*!
                 * \brief Add the given number of samples to the total number of samples on each level.
                 *
                 * \param numSamplesVec A `vector` of `int`s specifying the number of samples to take on each
                 * level
                 *
                 * \throw out_of_range If given vector is longer than the number of levels in this `MLMC`
                 */
                void AddSamples(const std::vector<int>& numSamplesVec);

                /*!
                 * \brief Update the statistics at the given level.
                 *
                 * This method is called by `AddSamples` to ensure that the given `MLMCBoxStats`
                 * contains the updated statistics of the given `MIMCMCBox`.
                 *
                 * \param stat A `shared_ptr` to the `MLMCBoxStats` that should be updated
                 * \param box A `shared_ptr` to the `MIMCMCBox` of which the statistics should be computed
                 *
                 * \note This function will only update the statistics `E0`, `dE`, `V0`, `dV` and `N`.
                 * The amount of work `W` should be handled by the calling function (i.e., `AddSamples`).
                 */
                void UpdateBoxStats(std::shared_ptr<MLMCBoxStats> const& stat,
                        std::shared_ptr<MIMCMCBox> const& box);

                // TODO: Maybe make the following functions static in the .cpp file
                // (they're just helper functions after all...)
                // Internal function for `UpdateBoxStats` that takes a `shared_ptr` to an `MLMCBoxStats` and
                // a `shared_ptr` to a `SampleCollection` for the fine and coarse samples.
                void _UpdateBoxStats(std::shared_ptr<MLMCBoxStats> const& stats,
                        std::shared_ptr<SampleCollection> const& fineSamples,
                        std::shared_ptr<SampleCollection> const& coarseSamples);

                /*!
                 * \brief Compute the MLMC rates alpha, beta and gamma
                 *
                 * The rate `alpha` is the so-called weak error rate, the decay of the expected value
                 * of the multilevel differences. The rate beta is the so-called strong error rate, the decay
                 * of the variance of the multilevel differences. The rate gamma is the cost complexity of the
                 * solver. These rates are frequently used to assess the performance of an MLMC estimator.
                 */
                void ComputeRates();

                // Same, but now use a maximum level L for continuation
                void ComputeRates(const int L);

                /*!
                 * \brief Compute the bias of the MLMC estimator.
                 */
                void ComputeBias();

                /*!
                 * \brief Compute the bias of the MLMC estimator using all levels between the given start and
                 * stop level.
                 *
                 * \param start The first level to use
                 * \param stop The last level to use
                 */
                void ComputeBias(const int start, const int stop);

                /*!
                 * \brief Compute the variance of this MLMC estimator.
                 */
                void ComputeVarianceOfEstimator();

                /*!
                 * \brief Compute the value of the mean square error splitting parameter (theta)
                 */
                void ComputeSplittingTheta();

                /*!
                 * \brief Computes the intercept and slope of the linear function through the given data points.
                 *
                 * \param x A vector with the x-coordinates of data points
                 * \param y A vector with the y-coordinates of the data points
                 * \return An `std::tuple` containing the intercept and the slope of the linear fit through the 
                 * given data points
                 */
                std::tuple<double, double> interp1(const std::vector<int>& x, const std::vector<double>& y);

                // Compute the constant part (sum) in the expression for the optimal number of samples
                double ComputeOptimalNumSamplesSum(const int L); 

                // Compute the optimal number of samples at a certain level
                double ComputeOptimalNumSamples(const int level, const double sum);

                // Used by `PrintBoxStat`
                std::string PrintBoxStat(std::shared_ptr<MLMCBoxStats> stat);

                // Print rates at the bottom of the `BoxStats` table
                std::string PrintRates();

                // Print a header for an MLMC simulation
                std::string PrintHeader();

                // Print a header that contains the given level
                std::string PrintLevelHeader(int level);

                // Print a footer for an MLMC simulation
                std::string PrintFooter();

                // Print a table with the optimal number of samples
                std::string PrintOptimalNumSamples(const std::vector<int>& additionalNumSamples, const int L);

                // Print the table header for the `PrintBoxStats` function
                std::string PrintTableHeader();

                // Print statistics about this MLMC estimator
                std::string PrintStatistics(int L);

                // Print a message to indicate convergence of the bias of the estimator
                std::string PrintBiasConvergence();

                // Print a message to indicate no convergence of the bias of the estimator
                std::string PrintNoBiasConvergence();

                // Print a message to indicate convergence of the statistical error of the estimator
                std::string PrintStatisticalErrorConvergence();

                // Print a message to indicate no convergence of the statistical error of the estimator
                std::string PrintNoStatisticalErrorConvergence();

                // Print a warning to indicate that the minimum number of levels is not reached
                std::string PrintMinLevelsNotReached();

                // Print a warning about a negativa alpha
                std::string WarnAlphaNegative();

                // Print a message to indicate no convergence of the bias of the estimator, and no more levels
                // are available
                std::string PrintNoBiasConvergenceNoMoreLevels();

                // Print a '+'-sign followed by `length` '-'-signs
                std::string hline(int length);

                // Same as `hline`, but repeat `nreps` times
                std::string hline(int length, int nreps);

                // Returns a string of length `width` with `str` in the center
                std::string center(int width, const std::string& str);
        };

        /*!
         * \brief Returns a string representation of an `MLMC`.
         *
         * \param stream The stream to add to
         * \param stat The `MLMC` to display
         * \return The stream with the `MLMC` added
         */
        std::ostream& operator<<(std::ostream& buff, const MLMC& mlmc);

    }
}

#endif

#include "MLMC.h"

namespace muq {
    namespace SamplingAlgorithms {

        // Constructor definitions
        EmptyCostModel::EmptyCostModel() : CostModel() {};

        std::ostream& operator<<(std::ostream& buff, const EmptyCostModel& costModel) {
            buff << "empty cost model";
            return buff;
        }

        GeometricCostModel::GeometricCostModel(double constant, double base, double exponent) : CostModel(), 
            constant(constant), base(base), exponent(exponent) {};

        // Evaluate the cost models
        double EmptyCostModel::evaluate(std::shared_ptr<MultiIndex> const& index) { return 0; };

        double GeometricCostModel::evaluate(std::shared_ptr<MultiIndex> const& index) {
            int level = index->GetValue(0);
            if (level == 0)
                return constant;
            else
                return constant * (std::pow(base, exponent * level) + std::pow(base, exponent * (level - 1)));
        }

    }
}

#include "MLMC.h"

namespace muq {
    namespace SamplingAlgorithms {

        // Print the statistics of all boxes
        std::string MLMC::PrintBoxStats() {
            return PrintBoxStats(maxNumLevels);
        }

        // Print the statistics of all boxes up to the given level
        std::string MLMC::PrintBoxStats(int numLevels) {
            // Print table header
            std::stringstream buff;
            buff << hline(7) << hline(16, 6) << hline(0) << std::endl;
            buff << PrintTableHeader();
            buff <<  hline(7) << hline(16, 6) << hline(0) << std::endl;
            // Print the statistics of each level
            for (int level = 0; level <= numLevels; level++) {
                buff << PrintBoxStat(stats[level]);
            }
            // Print an hline
            buff << hline(7) << hline(16, 6) << hline(0) << std::endl;
            return buff.str();
        }

        // Print the rates at the bottom of the table
        std::string MLMC::PrintRates() {
            std::stringstream buff;
            std::stringstream other_buff;
            other_buff << "alpha = " << std::fixed << std::setprecision(3) << alpha << " |";
            buff << std::setw(26) << '|' << std::setw(17) << other_buff.str();
            other_buff.str("");
            other_buff << "beta = " << std::fixed << std::setprecision(3) << beta << " |";
            buff << std::setw(17) << '|' << std::setw(17) << other_buff.str();
            other_buff.str("");
            other_buff << "gamma = " << std::fixed << std::setprecision(3) << gamma << " |";
            buff << std::setw(17) << other_buff.str() << std::endl;
            buff << std::setw(42) << hline(16) << hline(0);
            buff << std::setw(33) << hline(16) << hline(16) << hline(0) << std::endl;
            return buff.str();
        }

        // Print the statistics of a box at the given level
        std::string MLMC::PrintBoxStat(std::shared_ptr<MLMCBoxStats> stat) {
            std::stringstream buff;
            buff << "|" << std::setw(6) << stat->level->GetValue(0);
            buff << " |" << std::setw(15) << std::scientific << std::setprecision(7) << stat->E0;
            if (stat->level->GetValue(0) == 0)
                buff << " |" << std::setw(15) << '-';
            else
                buff << " |" << std::setw(15) << std::scientific << std::setprecision(7) << stat->dE;
            buff << " |" << std::setw(15) << std::scientific << std::setprecision(7) << stat->V0;
            if (stat->level->GetValue(0) == 0)
                buff << " |" << std::setw(15) << '-';
            else
                buff << " |" << std::setw(15) << std::scientific << std::setprecision(7) << stat->dV;
            buff << " |" << std::setw(15) << std::scientific << std::setprecision(7) << stat->W;
            buff << " |" << std::setw(15) << stat->N << " |" << std::endl;
            return buff.str();

        }

        // Print a header for an MLMC simulation
        std::string MLMC::PrintHeader() {
            std::stringstream buff;
            buff << hline(109) << hline(0) << std::endl;
            buff << '|' << std::setw(109) << std::left << " *** Running MLMC estimator " + name << '|' 
                << std::endl;
            time_t now = time(0);
            tm* t = localtime(&now);
            char date_time[100];
            std::strftime(date_time, 100, " *** Starting computation on %F @ %T", t);
            buff << '|' << std::setw(109) << std::left << date_time << '|' << std::endl;
            buff << "| *** Requested tolerance on the RMSE is " << std::left << std::setw(69)
                << std::scientific << std::setprecision(5) << targetTol << '|' << std::endl;
            buff << hline(109) << hline(0) << std::endl;
            return buff.str();
        }

        // Print statistics
        std::string MLMC::PrintStatistics(int L) {
            std::stringstream buff;
            buff << "The variance of the estimator is ";
            buff << std::scientific << std::setprecision(5) << varest << " (target is ";
            double varianceTarget = theta * std::pow(targetTol, 2);
            buff << std::scientific << std::setprecision(3) << varianceTarget << ")" << std::endl;
            buff << "The bias is ";
            buff << std::scientific << std::setprecision(5) << bias << " (target is ";
            double biasTarget = std::sqrt((1 - theta)) * targetTol;
            buff << std::scientific << std::setprecision(3) << biasTarget << ")" << std::endl;
            if (useVariableMSESplitting) {
                buff << "Value of the MSE splitting parameter is ";
                buff << std::fixed << std::setprecision(2) << theta << std::endl;
            }
            buff << "Estimated RMSE is ";
            buff << std::scientific << std::setprecision(5) << std::sqrt(varest + std::pow(bias, 2));
            buff << " (target is " << std::scientific << std::setprecision(3) << targetTol << ")" 
                << std::endl;
            if (alpha < 0)
                buff << WarnAlphaNegative();
            return buff.str();
        }

        // Print a header that contains the given level
        std::string MLMC::PrintLevelHeader(int level) {
            std::string str = "Running at level " + std::to_string(level);
            std::stringstream buff;
            buff << std::endl << str << std::endl;
            buff << std::string(str.length(), '^') << std::endl << std::endl;
            return buff.str();
        }

        // Print a footer for an MLMC simulation
        std::string MLMC::PrintFooter() {
            std::stringstream buff;
            buff << hline(109) << hline(0) << std::endl;
            time_t now = time(0);
            tm* t = localtime(&now);
            char date_time[100];
            std::strftime(date_time, 100, " *** Finished computation on %F @ %T", t);
            buff << '|' << std::setw(109) << std::left << date_time << '|' << std::endl;
            buff << '|' << std::setw(109) << std::left << " *** Successful termination" << '|' << std::endl;
            buff << hline(109) << hline(0) << std::endl;
            return buff.str();
        }

        // Print table with optimal number of samples
        std::string MLMC::PrintOptimalNumSamples(const std::vector<int>& additionalNumSamples, const int L) {
            std::stringstream buff;
            buff << hline(7) << hline(16) << hline(0) << std::endl;
            buff << '|' << center(7, "level") << '|' << center(16, "N_opt[l]") << '|' << std::endl;
            buff << hline(7) << hline(16) << hline(0) << std::endl;
            for (int level = 0; level <= L; level++) {
                int optimalNumSamples = additionalNumSamples[level] + stats[level]->N;
                if (optimalNumSamples > 0)
                    buff << "|" << std::setw(6) << level << " |" << std::setw(15) << optimalNumSamples << " |" 
                        << std::endl;

            }
            buff << hline(7) << hline(16) << hline(0) << std::endl;
            return buff.str();
        }
        
        // Prints the table header
        std::string MLMC::PrintTableHeader() {
            std::stringstream buff;
            buff << '|' << center(7, "level");
            std::string headers[6] = {"E[Q(l)]", "E[Q(l)-Q(l-1)]", "V[Q(l)]", "V[Q(l)-Q(l-1)]", "W[l]", "N[l]"};
            for (auto const& header: headers)
                buff << '|' << center(16, header);
            buff << '|' << std::endl;
            return buff.str();
        }

        // Print a message to indicate convergence of the bias of the estimator
        std::string MLMC::PrintBiasConvergence() {
            std::stringstream buff;
            buff << "Convergence reached (bias^2 = " << std::scientific << std::setprecision(5)
                << std::pow(bias, 2);
            buff << " < " << std::fixed << std::setprecision(2) << 1 - theta << " * targetTol^2 = "
                << std::scientific << std::setprecision(5) << (1 - theta) * std::pow(targetTol, 2) << ')'
                << std::endl;
            return buff.str();
        }

        // Print a message to indicate no convergence of the bias of the estimator
        std::string MLMC::PrintNoBiasConvergence() {
            std::stringstream buff;
            buff << "No convergence reached (bias^2 = " << std::scientific << std::setprecision(5)
                << std::pow(bias, 2);
            buff << " > " << std::fixed << std::setprecision(2) << 1 - theta << " * targetTol^2 = "
                << std::scientific << std::setprecision(5) << (1 - theta) * std::pow(targetTol, 2) << ')'
                << std::endl;
            buff << "Adding an extra level..." << std::endl;
            return buff.str();
        }

        // Print a message to indicate convergence of the bias of the statistical error
        std::string MLMC::PrintStatisticalErrorConvergence() {
            return "Statistical error constraint satisfied.\n";
        }

        // Print a message to indicate no convergence of the bias of the statistical error
        std::string MLMC::PrintNoStatisticalErrorConvergence() {
            return "Statistical error constraint not satisfied. Taking additional samples...\n";
        }

        // Print a warning to indicate that the minimum number of levels is not reached
        std::string MLMC::PrintMinLevelsNotReached() {
            return "The minimum number of levels is not reached.\n";
        }

        // Print a message to indicate no convergence of the bias of the estimator, and no more levels are
        // available
        std::string MLMC::PrintNoBiasConvergenceNoMoreLevels() {
            std::stringstream buff;
            buff << "No convergence reached (bias^2 = " << std::scientific << std::setprecision(5)
                << std::pow(bias, 2);
            buff << " > " << std::fixed << std::setprecision(2) << 1 - theta << " * targetTol^2 = "
                << std::scientific << std::setprecision(5) << (1 - theta) * std::pow(targetTol, 2) << ')' 
                << std::endl;
            buff << "Out of levels, the estimator is biased!" << std::endl;
            return buff.str();
        }

        // Print a warning about a negativa alpha
        std::string MLMC::WarnAlphaNegative() {
            return "Rate alpha is negative, bias estimate might be unreliable!\n";
        }

        // Prints a '+'-sign followed by `length` '-'-signs
        std::string MLMC::hline(int length) {
            std::string str = '+' + std::string(length, '-');
            return str;
        }
        
        // Same as `hline`, but repeat `nreps` times
        std::string MLMC::hline(int length, int nreps) {
            std::stringstream buff;
            for (int rep = 0; rep < nreps; rep++)
                buff << hline(length);
            return buff.str();
        }
        
        // Returns a string of length `width` with `str` in the center
        std::string MLMC::center(int width, const std::string& str) {
            int length = str.length();
            if (width < length)
                return str;

            int diff = width - length;
            int lpad = diff/2;
            int rpad = diff - lpad;
            return std::string(lpad, ' ') + str + std::string(rpad, ' ');
        }

    }
}

#include "MLMC.h"

namespace muq {
    namespace SamplingAlgorithms {

        // Override from SamplingAlgorithm
        std::shared_ptr<SampleCollection> MLMC::GetSamples() const { return nullptr; }

        // Override from SamplingAlgorithm
        std::shared_ptr<SampleCollection> MLMC::GetQOIs() const { return nullptr; }

        // Adds samples to each of the boxes
        // TODO: In parallel implementation, this is the (only?) function that should be changed
        void MLMC::AddSamples(const std::vector<int>& numSamplesVec) {
            for (std::size_t level = 0; level < numSamplesVec.size(); ++level) {
                int numSamplesToAdd = numSamplesVec[level];
                if (numSamplesToAdd > 0) {
                    if (isVerbose) {
                        std::string str = (stats[level]->N != 0) ? "additional" : "warm-up";
                        std::cout << "Taking " << numSamplesToAdd << ' ' << str << " samples on level ";
                        std::cout << level << "..." << std::flush;
                    }
                    auto tic = std::chrono::steady_clock::now(); // Time execution with std::chrono (TODO: not
                                                                 // sure if this is the recommended way,
                                                                 // perhapse we could use the metadata attached
                                                                 // to each sample?)
                    auto box = GetBox(level);
                    for (int n = 0; n < numSamplesToAdd; n++) { // Sequentially add samples to the box
                        box->Sample();
                    }
                    auto toc = std::chrono::steady_clock::now();
                    std::chrono::duration<double> elapsed = toc - tic;
                    auto stat =  GetBoxStats(level);
                    stat->elapsed += elapsed.count(); // Update elpased run time on this level
                    UpdateBoxStats(stat, box); // Update box statistics
                    if (isVerbose)
                        std::cout << " done" << std::endl;
                }
            }
        }

        // Update statistics of the given `MLMCBoxStats` using the samples in the given `MIMCMCBox`
        void MLMC::UpdateBoxStats(std::shared_ptr<MLMCBoxStats> const& stat,
                std::shared_ptr<MIMCMCBox> const& box) {
            auto fineSamples = box->GetChain(std::make_shared<MultiIndex>(1, 0))->GetQOIs();
            auto coarseSamples = box->GetChain(std::make_shared<MultiIndex>(1, 0))->GetQOIs();
            if (stat->level->GetValue(0) > 0) // The idea here is to use the "same" fineSamples for level 0 only.
                                 // This way, we can use the same function `_UpdateBoxStats` on all levels.
                fineSamples = box->GetChain(std::make_shared<MultiIndex>(1, 1))->GetQOIs();
            _UpdateBoxStats(stat, fineSamples, coarseSamples);
        }

        // Internal function for Update statistics
        void MLMC::_UpdateBoxStats(std::shared_ptr<MLMCBoxStats> const& stat,
                std::shared_ptr<SampleCollection> const& fineSamples,
                std::shared_ptr<SampleCollection> const& coarseSamples) {
            auto numSamples = fineSamples->size();
            // Use the Two-pass algorithm to compute mean and variance
            // (see, e.g., https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance)
            double sumQOIs = 0;
            double sumDiff = 0;
            for (std::size_t n = 0; n < numSamples; ++n) {
                double Qf = fineSamples->at(n)->state[0](0);
                double DeltaQ = Qf - coarseSamples->at(n)->state[0](0);
                sumQOIs += Qf;
                sumDiff += DeltaQ;
            }
            double meanQOIs = sumQOIs / numSamples;
            double meanDiff = sumDiff / numSamples;

            sumQOIs = 0;
            sumDiff = 0;
            for (std::size_t n = 0; n < numSamples; ++n) {
                double Qf = fineSamples->at(n)->state[0](0);
                double DeltaQ = Qf - coarseSamples->at(n)->state[0](0);
                sumQOIs += (Qf - meanQOIs) * (Qf - meanQOIs);
                sumDiff += (DeltaQ  - meanDiff) * (DeltaQ - meanDiff);
            }

            // Update the `MLMCBoxStats`
            stat->E0 = meanQOIs;
            stat->V0 = sumQOIs / (numSamples - 1); 
            if (stat->level->GetValue(0) == 0) { // For convenience, now `dV` is the same as `V0` at level 0
                stat->dE = meanQOIs;
                stat->dV = sumQOIs / (numSamples - 1); 
            } else {
                stat->dE = meanDiff;
                stat->dV = sumDiff / (numSamples - 1); 
            }
            stat->N = numSamples;
            if (typeid(*costModel) == typeid(EmptyCostModel)) // Update cost at this level using actual run time
                stat->W = stat->elapsed / stat->N;
            else // or using the user-provided cost model
                stat->W = costModel->evaluate(stat->level);
        }

        // Returns the box at the given level
        // TODO: No out-of-bounds checking
        std::shared_ptr<MIMCMCBox> MLMC::GetBox(int level) {
            return boxes[level];
        }

        // Returns the stats at the given level
        // TODO: No out-of-bounds checking
        std::shared_ptr<MLMCBoxStats> MLMC::GetBoxStats(int level) {
            return stats[level];
        }
        
        // Compute rates alpha, beta and gamma from MLMC convergence theorem
        void MLMC::ComputeRates() {
            ComputeRates(maxNumLevels - 1);
        }

        void MLMC::ComputeRates(const int L) {
            std::vector<int> x;
            std::vector<double> y_alpha;
            std::vector<double> y_beta;
            std::vector<double> y_gamma;
            for (int level = 1; level <= L; level++) {
                auto stat = stats[level];
                if (stat->N > 0) {
                    x.push_back(level);
                    y_alpha.push_back(std::log2(std::abs(stat->dE)));
                    y_beta.push_back(std::log2(stat->dV));
                    y_gamma.push_back(std::log2(stat->W / stat->N));
                }
            }
            std::tie(a, alpha) = interp1(x, y_alpha);
            alpha = -alpha;
            std::tie(std::ignore, beta) = interp1(x, y_beta);
            beta = -beta;
            std::tie(std::ignore, gamma) = interp1(x, y_gamma);
        }

        // Compute bias using all levels
        void MLMC::ComputeBias() {
            int stop;
            for (stop = 0; stop < maxNumLevels; stop++)
                if (stats[stop]->N == 0)
                    break;
            ComputeBias(1, stop - 1);
        }

        // Compute the bias use the levels between start and stop
        void MLMC::ComputeBias(const int start, const int stop) {
            if (alpha < 0) {
                bias = stats[stop]->dE / (std::pow(2, 0.5) - 1);
            }
            if (useGilesBiasEstimate) {
                double Y = stats[stop]->dE;
                if (stop - start > 0) {
                    double otherY = stats[stop-1]->dE*std::pow(2, -alpha);
                    Y = (Y > otherY) ? Y : otherY;
                }
                bias = Y / (std::pow(2, alpha) - 1);
            } else {
                bias =  std::pow(2, a - alpha*stop) / (std::pow(2, alpha) - 1);
            }
            bias = std::abs(bias);
        }

        // Compute the variance of this MLMC estimator.
        void MLMC::ComputeVarianceOfEstimator() {
            varest = 0;
            for (int level = 0; level < maxNumLevels; level++)
                if (stats[level]->N > 0)
                    varest += stats[level]->dV / stats[level]->N;
        }

        void MLMC::ComputeSplittingTheta() {
            ComputeRates(); // Rate alpha is needed for bias estimation (using all levels)
            ComputeBias(); // Compute the "true" bias in the model (using all levels where samples are available)
            theta = std::min(theta_max, std::max(theta_min, 1 - std::pow(bias, 2) / std::pow(targetTol, 2)));
        }

        // Linear regression
        std::tuple<double, double> MLMC::interp1(const std::vector<int>& x, const std::vector<double>& y) {
            double xbar = std::accumulate(x.begin(), x.end(), 0.0) / x.size();
            double ybar = std::accumulate(y.begin(), y.end(), 0.0) / y.size();
            double num = 0;
            double denum = 0;
            for (int i = 0; i < x.size(); i++) {
                num += (x[i] - xbar)*(y[i] - ybar);
                denum += (x[i] - xbar)*(x[i] - xbar);
            }
            double b = num / denum;
            double a = ybar - b * xbar;
            return std::make_tuple(a, b);
        }

        // Compute the constant part (sum) in the expression for the optimal number of samples
        double MLMC::ComputeOptimalNumSamplesSum(const int L) {
            double sum = 0;
            for (int level = 0; level <= L; level++) {
                auto stat = stats[level];
                sum += std::sqrt(stat->dV * stat->W);
            }
            return sum;
        }

        // Compute the optimal number of samples at a certain level
        double MLMC::ComputeOptimalNumSamples(const int level, const double sum) {
            auto stat = stats[level];
            return (int) std::ceil(1 / (theta * targetTol * targetTol) * std::sqrt(stat->dV / stat->W) * sum);
        }

        // Adds an `MLMCBoxStats` to the given stream (used by `PrintBoxStats`)
        std::ostream& operator<<(std::ostream& buff, std::shared_ptr<MLMCBoxStats> stat) {
            // TODO: Return some more details...
            buff << "A BoxStat.";
            return buff;
        }

        // Adds an `MLMC` to the given stream
        // TODO: Return some more details...
        std::ostream& operator<<(std::ostream& buff, const MLMC& mlmc) {
            buff << "An MLMC method.";
            return buff;
        }

    }
}

#include <iostream>

#include "UQProblem.h"
#include "MLMC.h"

int main(int argc, char **argv) {

	pt::ptree pt;
        pt.put("numWarmUpSamples", 51);
        pt.put("targetTol", 5e-6);
        pt.put("numSamplesFraction", 0);
	auto componentFactory = std::make_shared<MyMIComponentFactory>(pt);
        // TODO: add cost model to the ComponentFactory (?)
        auto costModel = std::make_shared<EmptyCostModel>();
        //auto costModel = std::make_shared<GeometricCostModel>(1, 2, 2);
	MLMC mlmc(pt, componentFactory, costModel);

        mlmc.Run();

	return 0;

}

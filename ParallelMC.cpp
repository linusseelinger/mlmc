
#include <iostream>
#include "Sandwich.hpp"
#include <random>


#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/DummyKernel.h"
#include "MUQ/SamplingAlgorithms/MHProposal.h"
#include "MUQ/SamplingAlgorithms/MHKernel.h"
#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SingleChainMCMC.h"
#include "MUQ/SamplingAlgorithms/MCMCFactory.h"
#include "MUQ/SamplingAlgorithms/ParallelFixedSamplesMIMCMC.h"

#include <boost/property_tree/ptree.hpp>

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;

#include "MCSampleProposal.h"
#include "UQProblem.h"


int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    pt::ptree pt;
    pt.put("NumSamples_0", 5e2);
    pt.put("NumSamples_1", 1e2);
    pt.put("NumSamples_2", 5e1);
    pt.put("MCMC.BurnIn", 1);
    pt.put("MLMCMC.Subsampling", 1);
    pt.put("PrintLevel",3);

    auto comm = std::make_shared<parcer::Communicator>();

    auto componentFactory = std::make_shared<MyMIComponentFactory>(pt);
    StaticLoadBalancingMIMCMC parallelMIMCMC (pt, componentFactory);

    if (comm->GetRank() == 0) {
      parallelMIMCMC.Run();
      Eigen::VectorXd meanQOI = parallelMIMCMC.MeanQOI();
      std::cout << "mean QOI: " << meanQOI.transpose() << std::endl;
      parallelMIMCMC.WriteToFile("ParallelMC_samples.h5");
    }
    parallelMIMCMC.Finalize();

    MPI_Finalize();

    return 0;
}

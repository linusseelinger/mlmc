//
// Sandwich.hpp
// Computes the displacement and stress of a sandwich beam.
//
// The Young's modulus of the beam has the following layout:
//
//        +-----------------------------------------------------+
//        |                          Ed1                        |
//        +-----------------------------------------------------+
//        |                                                     |
//        |                          Ed2                        |
//        |                                                     |
//        +-----------------------------------------------------+
//        |                          Ed1                        |
//        +-----------------------------------------------------+
//
// Use `setYoungsModulus(Ed1, Ed2)` to set the values of the Young's modulus of the beam.
// Default values are Ed1 = 200GPa, Ed2 = 30GPa
//
// The beam can be fixed on one side (cantilever beam) or on both sides (clamped beam).
//
//      |> +---------------------+       |> +---------------------+ <|
//      |> +                     +   or  |> +                     + <|
//      |> +---------------------+       |> +---------------------+ <|
//
// Use `setCantilever()` (default) or `setClamped()` to set the load case.
//
// The beam is subject to a distributed load on top, or a point load on the right.
//
//        | | | | | | | | | | | |                               |
//        v v v v v v v v v v v v                               v
//        +---------------------+         +---------------------+
//        +                     +   or    +                     +
//        +---------------------+         +---------------------+
//
// Use `setDistributedLoad()` or `setPointLoad()` (default) to set the load case.
//
// @author: Pieterjan Robbe
// @date: 27/04/20
//

#ifndef Sandwich_hpp
#define Sandwich_hpp

#include <iostream>
#include <fstream>
#include <assert.h>
#include <stdexcept>
#include <cmath>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>


using namespace std;
using namespace Eigen;

template <class T> // T is the numeric float type
class Sandwich {
    
    // define commonly used Eigen types with template parameter T
    using MatrixXt = Matrix<T, Dynamic, Dynamic>;
    using Matrix4t = Matrix<T, 4, 4>;
    using Matrix3t = Matrix<T, 3, 3>;
    using Matrix34t = Matrix<T, 3, 4>;
    using VectorXt = Matrix<T, Dynamic, 1>;
    using RowVectorXt = Matrix<T, 1, Dynamic>;
    
    const T Lx; // beam width
    const T Ly; // beam height
    const int nely; // number of elements in y-direction
    const int nelx; // number of elements in x-direction
    MatrixXt Ed; // Young's modulus of the beam (matrix of size nely x nelx)
    MatrixXt nu; // (square of the) Poissoin ratio of the beam (matrix of size nely x nelx)
    int freedofstart, freedoflen; // determines the free degrees of freedom
    MatrixXi Fdof; // degrees of freedom where the dorce is applied
    VectorXt Fval; // value of the force in the force degrees of freedom Fdof
    VectorXt u; // allocate displacement vector
    MatrixXi edofMat; // allocate matrix for element degrees of freedom
    MatrixXi enodeMat; // allocate matrix for element degrees of freedom
    MatrixXi nele; // number of (neighbour) elements for each node
    MatrixXt B1; MatrixXt B2; MatrixXt B3; MatrixXt B4; // stress submatrices
    VectorXi iK; // allocate vector for i-indices
    VectorXi jK; // allocate vector for j-indices
    MatrixXt s; // allocate matrix for stresses

public:

    // constructor
    Sandwich(int nely_ = 10) :
        Lx(1),
        Ly(.25),
        nely(nely_),
        nelx(Lx/(Ly/nely)),
        Ed(nely, nelx),
        nu(MatrixXt::Constant(nely, nelx, .25)),
        u(2*(nelx + 1)*(nely + 1), 1),
        s(3, (nelx + 1)*(nely + 1))
    {
        assert(check_n(nely_));
        
        // apply default settings
        setYoungsModulus(200e9, 30e9);
        setCantileverBeam();
        setPointLoad();
        
        // precompute stuff for faster repeated computation of stiffness matrix and stresses
        computeEdofMat(); // matrix with degrees of freedom for each element
        computeEnodeMat(); // matrix with nodes for each element
        computeNeighbors(); // number of (neighbour) elements for each node
        computeB(); // stress submatrices
        computeIndices(); // i- and j-indices of sparse stiffness matrix K
    }

	//Sandwich<T>& operator=(const Sandwich<T>&) = default;

    // set Youngs modulus
    void setYoungsModulus(T Ed1, T Ed2) {
        int layerwidth = (int) nely / 5;
        Ed.setConstant(Ed2); // middle layer
        Ed.topRows(layerwidth).setConstant(Ed1); // top layer
        Ed.bottomRows(layerwidth).setConstant(Ed1); // bottom layer
    }
    
    // set Youngs modulus (spatially varying)
    void setSpatialVaryingYoungsModulus(VectorXd x) { setSpatialVaryingYoungsModulus(200e9, 30e9, x); }
    
    void setSpatialVaryingYoungsModulus(T Ed1, T Ed2, VectorXd x) {
        setYoungsModulus(Ed1, Ed2); // set Young's modulus to non-varying default
        VectorXt v1(nely);
        RowVectorXt v2(nelx);
        MatrixXt v = MatrixXt::Zero(nely, nelx);
        long szx = x.size();
        int nterms = (int) ceil(sqrt(szx)), termno;
        for (int ix=0; ix < nterms; ix++) {
            v1 = pow(ix + 1, -.5) * VectorXt::LinSpaced(nely, 1/nely, (ix+1)*M_PI - 1/nely).array().sin();
            for (int iy=0; iy < nterms; iy++) {
                v2 = pow(iy + 1, -.5) * VectorXt::LinSpaced(nelx, 1/nelx, (iy+1)*M_PI - 1/nelx).array().sin();
                termno = (ix+1)*(iy+1);
                if (termno < szx) {
                    v += x(termno - 1) * v1 * v2;
                }
            }
        }
        v = 1 + 0.01*v.array().exp();
        Ed = Ed.cwiseProduct(v);
    }
    
    // set Poisson ration
    void setPoissonRatio(T nu_) {
        nu.setConstant(nu_);
    }
    
    // set fixed degrees of freedom to cantilever beam configuration
    void setCantileverBeam() {
        freedofstart = 2*(nely + 1);
        freedoflen = 2*(nely + 1)*nelx;
    }
    
    // set fixed degrees of freedom to clamped beam configuration
    void setClampedBeam() {
        freedofstart = 2*(nely + 1);
        freedoflen = 2*(nely + 1)*(nelx - 1);
    }
    
    // set load configuration to distributed load at the top
    void setDistributedLoad() {
        Fdof.resize(nelx + 1, 1);
        Fval.resize(nelx + 1, 1);
        Fdof(0) = 1;
        Fval(0) = -5e6/nelx;
        for (int k=1; k < nelx; k++) {
            Fdof(k) = 2*(nely + 1)*k + 1;
            Fval(k) = -1e7/nelx;
        }
        Fdof(nelx) = 2*(nely + 1)*nelx + 1;
        Fval(nelx) = -5e6/nelx;
    }
    
    // set load configuration to point load
    void setPointLoad() {
        Fdof.resize(nely + 1, 1);
        Fval.resize(nely + 1, 1);
        Fdof(0) = 2*nelx*(nely + 1) + 1;
        Fval(0) = -5e6/nely;
        for (int k=1; k < nely; k++) {
            Fdof(k) = 2*(nelx*(nely + 1) + k) + 1;
            Fval(k) = -1e7/nely;
        }
        Fdof(nely) = 2*(nelx*(nely + 1) + nely) + 1;
        Fval(nely) = -5e6/nely;
    }
    
    // getter for the displacement u
    VectorXt getDisplacement() { return u; }
    
    // getter for the stress s
    VectorXt getStress() { return s; }
    
    // planestress4
    //
    // This is the main function that computes the displacement of and stress inside the beam.
    //
    void planeStress4() {
        computeDisplacement();
        computeStress();
    }
    
    // compute the displacement of the beam
    void computeDisplacement() {
        int ndof = 2*(nelx + 1)*(nely + 1); // total number of degrees of freedom
        int nentries = 64*nelx*nely; // total number of entries in stiffness matrix
        
        // compose stiffness sub matrices
        Matrix4t A11;
        A11 << 12, 3, -6, -3, 3, 12, 3, 0, -6, 3, 12, -3, -3, 0, -3, 12;
        Matrix4t A12;
        A12 << -6, -3, 0, 3, -3, -6, -3, -6, 0, -3, -6, 3, 3, -6, 3, -6;
        Matrix4t B11;
        B11 << -4, 3, -2, 9, 3, -4, -9, 4, -2, -9, -4, -3, 9, 4, -3, -4;
        Matrix4t B12;
        B12 << 2, -3, 4, -9, -3, 2, 9, -2, 4, 9, 2, 3, -9, -2, 3, 2;
        MatrixXt A(8, 8);
        A << A11, A12, A12.transpose(), A11;
        MatrixXt B(8, 8);
        B << B11, B12, B12.transpose(), B11;
        
        // compute values in stiffness matrix
        Map<RowVectorXt> nuView(nu.data(), 1, nelx*nely);
        Map<RowVectorXt> EdView(Ed.data(), 1, nelx*nely);
        RowVectorXt sKtemp;
        sKtemp = EdView.cwiseProduct((1 - nuView.array().square()).matrix().cwiseInverse() / 24);
        Map<VectorXt> AView(A.data(), 64, 1);
        Map<VectorXt> BView(B.data(), 64, 1);
        MatrixXt sKe = AView * sKtemp + BView * (nuView.cwiseProduct(sKtemp));
        Map<VectorXt> sK(sKe.data(), nentries);
        
        // finally, construct the sparse matrix
        vector<Triplet<T>> myentries; // create triplet list <i, j, K_ij>
        myentries.reserve(nentries);
        for (int k=0; k < nentries; k++) {
            myentries.push_back(Triplet<T>(iK(k), jK(k), sK(k)));
        }
        SparseMatrix<T> K(ndof, ndof);
        K.setFromTriplets(myentries.begin(), myentries.end()); // create sparse vector from triplets
        
        // construct right-hand side
        VectorXt F = VectorXt::Constant(ndof, 1, 0);
        for (int k=0; k < Fdof.size(); k++) {
            F(Fdof(k)) = Fval(k);
        }
        
        // solve for displacements u
        SimplicialLDLT<SparseMatrix<T>> solver; // solver based on Cholesky factorization
        solver.compute(K.block(freedofstart, freedofstart, freedoflen, freedoflen)); // compute cholesky factorization
        if (solver.info() != Success) { throw runtime_error("decomposition failed"); }
        u.setConstant(ndof, 1, 0);
        u.segment(freedofstart, freedoflen) = solver.solve(F.segment(freedofstart, freedoflen)); // compute solution
        if (solver.info() != Success) { throw runtime_error("solving failed"); }
    }
    
    // compute the stress inside the beam
    void computeStress() {
        MatrixXt ue(nelx*nely, 8);
        for (int col=0; col<8; col++) {
            for (int row=0; row<nelx*nely; row++) {
                ue(row, col) = u(edofMat(row, col));
            }
        }
        ue.transposeInPlace();
        s.setConstant(3, (nelx + 1)*(nely + 1), 0);
        Matrix3t D(3, 3);
        Matrix34t temp;
        int inde;
        for (int j=0; j<nelx; j++) {
            for (int i=0; i<nely; i++) {
                D.setZero();
                D << 1, nu(i, j), 0, nu(i, j), 1, 0, 0, 0, (1 - nu(i, j))/2;
                D *= Ed(i, j) / (1 - pow(nu(i, j), 2));
                
                inde = j*nely + i;
                
                temp.col(0) = D*B1*ue.col(inde);
                temp.col(1) = D*B2*ue.col(inde);
                temp.col(2) = D*B3*ue.col(inde);
                temp.col(3) = D*B4*ue.col(inde);
                
                s(all, enodeMat.row(inde).array()) +=
                    nele.row(inde).template cast<T>().replicate(3, 1).cwiseInverse().cwiseProduct(temp);
            }
        }
    }
    
    // save the displacement and stress to file for plotting
    void writeToFile() {
        writeYoungsModulusToFile("youngs_modulus.txt");
        writeDisplacementToFile("displacement.txt");
        writeStressToFile("stress.txt");
    }
    
private:
    
    bool check_n(int n){ return (n > 0 && n % 5 == 0); }
    
    //  compose matrix with degrees of freedom for each element
    void computeEdofMat() {
        int ndof2 = (nelx + 1)*(nely + 1);
        MatrixXi nodenrs(nely + 1, nelx + 1);
        nodenrs.reshaped() = VectorXi::LinSpaced(ndof2, 1, ndof2);
        MatrixXi nodersh = nodenrs.topLeftCorner(nely, nelx).eval();
        Map<VectorXi> edofVec(nodersh.data(), nelx*nely, 1);
        edofVec *= 2;
        MatrixXi temp(1, 8);
        temp << 0, 1, 2*nely + 2, 2*nely + 3, 2*nely, 2*nely + 1, -2, -1;
        edofMat = edofVec.replicate(1, 8) + temp.replicate(nelx*nely, 1);
    }
    
    // compose matrix with nodes for each element
    void computeEnodeMat() {
        int ndof2 = (nelx + 1)*(nely + 1);
        MatrixXi nodenrs(nely + 1, nelx + 1);
        nodenrs.reshaped() = VectorXi::LinSpaced(ndof2, 1, ndof2);
        MatrixXi nodersh = nodenrs.bottomLeftCorner(nely, nelx).eval();
        Map<VectorXi> enodeVec(nodersh.data(), nelx*nely, 1);
        enodeVec -= VectorXi::Constant(nelx*nely, 1, 1);
        MatrixXi temp(1, 4);
        temp << 0, nely + 1, nely, -1;
        enodeMat = enodeVec.replicate(1, 4) + temp.replicate(nelx*nely, 1);
    }
    
    // number of (neighbour) elements for each node
    void computeNeighbors() {
        MatrixXi neighb = MatrixXi::Constant(nely + 1, nelx + 1, 4);
        neighb.row(0).setConstant(2);
        neighb.row(nely).setConstant(2);
        neighb.col(0).setConstant(2);
        neighb.col(nelx).setConstant(2);
        neighb(0, 0) = 1; neighb(nely, 0) = 1; neighb(0, nelx) = 1; neighb(nely, nelx) = 1;
        neighb.resize((nely + 1)*(nelx + 1), 1);nele.resize(nelx*nely, 4);
        for (int col=0; col<4; col++) {
            for (int row=0; row<nelx*nely; row++) {
                nele(row, col) = neighb(enodeMat(row, col));
            }
        }
    }
    
    // compute stress submatrices
    void computeB() {
        T he = Ly / nely;
        T dN1x12 = -1/he; T dN2x12 = 1/he; T dN3x12 = 0; T dN4x12 = 0;
        T dN1x34 = 0; T dN2x34 = 0; T dN3x34 = 1/he; T dN4x34 = -1/he;
        T dN1y14 = -1/he; T dN2y14 = 0; T dN3y14 = 0; T dN4y14 = 1/he;
        T dN1y23 = 0; T dN2y23 = -1/he; T dN3y23 = 1/he; T dN4y23 = 0;
        
        B1.resize(3, 8);
        B1 << dN1x12, 0,      dN2x12, 0,      dN3x12, 0,      dN4x12, 0,
              0,      dN1y14, 0,      dN2y14, 0,      dN3y14, 0,      dN4y14,
              dN1y14, dN1x12, dN2y14, dN2x12, dN3y14, dN3x12, dN4y14, dN4x12;
        B2.resize(3, 8);
        B2 << dN1x12, 0,      dN2x12, 0,      dN3x12, 0,      dN4x12, 0,
              0,      dN1y23, 0,      dN2y23, 0,      dN3y23, 0,      dN4y23,
              dN1y23, dN1x12, dN2y23, dN2x12, dN3y23, dN3x12, dN4y23, dN4x12;
        B3.resize(3, 8);
        B3 << dN1x34, 0,      dN2x34, 0,      dN3x34, 0,      dN4x34, 0,
              0,      dN1y23, 0,      dN2y23, 0,      dN3y23, 0,      dN4y23,
              dN1y23, dN1x34, dN2y23, dN2x34, dN3y23, dN3x34, dN4y23, dN4x34;
        B4.resize(3, 8);
        B4 << dN1x34, 0,      dN2x34, 0,      dN3x34, 0,      dN4x34, 0,
              0,      dN1y14, 0,      dN2y14, 0,      dN3y14, 0,      dN4y14,
              dN1y14, dN1x34, dN2y14, dN2x34, dN3y14, dN3x34, dN4y14, dN4x34;
    }
    
    // compute i- and j-indices in stiffness matrix
    void computeIndices() {
        int nentries = 64*nelx*nely;
        iK.resize(nentries);
        jK.resize(nentries);
        for (int row=0; row<nelx*nely; row++) {
            for (int col=0; col<8; col++) {
                iK.segment(row*64 + col*8, 8) = edofMat.row(row);
                jK.segment(row*64 + col*8, 8) = VectorXi::Constant(8, 1, edofMat(row, col));
            }
        }
    }
    
    void writeYoungsModulusToFile(const string& filename) {
        std::ofstream file(filename);
        if (file.is_open()) {
            file << Lx << endl;
            file << Ly << endl;
            file << nelx << endl;
            file << nely << endl;
            file << Map<VectorXt>(Ed.data(), Ed.size()) << endl;
        }
        file.close();
    }
    
    void writeDisplacementToFile(const string& filename) {
        std::ofstream file(filename);
        if (file.is_open()) {
            file << Lx << endl;
            file << Ly << endl;
            file << nelx << endl;
            file << nely << endl;
            file << u << endl;
        }
        file.close();
    }
    
    void writeStressToFile(const string& filename) {
        std::ofstream file(filename);
        if (file.is_open()) {
            file << Lx << endl;
            file << Ly << endl;
            file << nelx << endl;
            file << nely << endl;
            file << s << endl;
        }
        file.close();
    }
    
    friend ostream& operator<<(ostream& os, const Sandwich& s) {
        os << "beam of height " << s.Ly << " and width " << s.Lx << " with " << s.nelx << "x" << s.nely << " degrees of freedom." << endl;
        return os;
    };
};

#endif /* Sandwich_hpp */


#include <iostream>
#include "Sandwich.hpp"
#include <random>


#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/DummyKernel.h"
#include "MUQ/SamplingAlgorithms/MHProposal.h"
#include "MUQ/SamplingAlgorithms/MHKernel.h"
#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SingleChainMCMC.h"
#include "MUQ/SamplingAlgorithms/MCMCFactory.h"
#include "MUQ/SamplingAlgorithms/ParallelFixedSamplesMIMCMC.h"
#include "MUQ/SamplingAlgorithms/MIMCMC.h"

#include <boost/property_tree/ptree.hpp>

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;

#include "MCSampleProposal.h"
#include "UQProblem.h"


int main(int argc, char **argv) {
    // This code is actually sequential; we use a parallelizable model though, so let's just init MPI as usual
    MPI_Init(&argc, &argv);
    
    pt::ptree pt;
    pt.put("NumSamples_0", 10);
    pt.put("NumSamples_1", 10);
    pt.put("NumSamples_2", 10);
    pt.put("MCMC.BurnIn", 1);
    pt.put("MLMCMC.Subsampling", 1);
    pt.put("PrintLevel",3);

    auto comm = std::make_shared<parcer::Communicator>();

    auto componentFactory = std::make_shared<MyMIComponentFactory>(pt);
    MIMCMC mimcmc (pt, componentFactory);

    mimcmc.Run();
    mimcmc.WriteToFile("SequentialMLMCSamples.h5");

    // Get box of index 1
    std::shared_ptr<MIMCMCBox> box = mimcmc.GetBox(std::make_shared<MultiIndex>(1,0));
    // Access its coarsest chain's samples
    box->GetChain(std::make_shared<MultiIndex>(0,0))->GetSamples();

    Eigen::VectorXd meanQOI = mimcmc.MeanQOI();
    std::cout << "mean QOI: " << meanQOI.transpose() << std::endl;

    MPI_Finalize();
    return 0;
}

#include "MUQ/Modeling/Distributions/Gaussian.h"
#include "MUQ/Modeling/Distributions/Density.h"

#include "MUQ/SamplingAlgorithms/DummyKernel.h"
#include "MUQ/SamplingAlgorithms/MHProposal.h"
#include "MUQ/SamplingAlgorithms/MHKernel.h"
#include "MUQ/SamplingAlgorithms/SamplingProblem.h"
#include "MUQ/SamplingAlgorithms/SingleChainMCMC.h"
#include "MUQ/SamplingAlgorithms/MCMCFactory.h"
#include "MUQ/SamplingAlgorithms/ParallelFixedSamplesMIMCMC.h"
#include "MUQ/SamplingAlgorithms/MIMCMC.h"

#include "MCSampleProposal.h"

#include "MUQ/SamplingAlgorithms/ParallelizableMIComponentFactory.h"
#include "MUQ/SamplingAlgorithms/SubsamplingMIProposal.h"

#include <boost/property_tree/ptree.hpp>
#include "Sandwich.hpp"

namespace pt = boost::property_tree;
using namespace muq::Modeling;
using namespace muq::SamplingAlgorithms;
using namespace muq::Utilities;

class MySamplingProblem : public AbstractSamplingProblem {
public:
  MySamplingProblem(std::shared_ptr<MultiIndex> const& index)
   : AbstractSamplingProblem(Eigen::VectorXi::Constant(1,121), Eigen::VectorXi::Constant(1,1)),
     index(index),
     elements(10 * pow(2, index->GetValue(0))), // changed to geometrical structure
     S(elements)
  {
    std::cout << "Index " << *index << " with elements " << elements << std::endl;
  }

  virtual ~MySamplingProblem() = default;


  virtual double LogDensity(std::shared_ptr<SamplingState> const& state) override {

	auto myState = state->state[0];

	//std::cout << "  >>> set Young's modulus to [" << myState(0) << ", " << myState(1) << ", " << myState(2) << ", ... ]" << std::endl;

    S.setSpatialVaryingYoungsModulus(myState);

    return 0;
  }

  virtual std::shared_ptr<SamplingState> QOI() override {
    S.computeDisplacement();
    //std::cout << "computing qoi for " << S << std::endl;
    Eigen::VectorXd last_displacement(1);
    last_displacement << S.getDisplacement()[S.getDisplacement().size() - 1];

    return std::make_shared<SamplingState>(last_displacement, 1.0);
  }

private:
  std::shared_ptr<MultiIndex> index;
  int elements;
  Sandwich<double> S;
};


class MyInterpolation : public MIInterpolation {
public:
  std::shared_ptr<SamplingState> Interpolate (std::shared_ptr<SamplingState> const& coarseProposal, std::shared_ptr<SamplingState> const& fineProposal) {
    return std::make_shared<SamplingState>(coarseProposal->state);
  }
};

class MyMIComponentFactory : public ParallelizableMIComponentFactory {
public:
  MyMIComponentFactory (pt::ptree pt)
   : pt(pt)
  { }

  virtual bool IsInverseProblem() override {
    return false;
  }

  void SetComm(std::shared_ptr<parcer::Communicator> const& comm) override {

  }

  virtual std::shared_ptr<MCMCProposal> Proposal (std::shared_ptr<MultiIndex> const& index, std::shared_ptr<AbstractSamplingProblem> const& samplingProblem) override {

    // TODO: For multilevel, may need to define proposals depending on level here?

    // Set up a Gaussian
    Eigen::VectorXd mu_prop = Eigen::VectorXd::Zero(121);
    Eigen::MatrixXd cov_prop = Eigen::MatrixXd::Identity(121,121);
    auto proposalDensity = std::make_shared<Gaussian>(mu_prop, cov_prop);

    // Let's use a proposal that only samples from the given density
    return std::make_shared<MCSampleProposal>(pt, samplingProblem, proposalDensity);
  }

  virtual std::shared_ptr<MultiIndex> FinestIndex() override {
    auto index = std::make_shared<MultiIndex>(1);
    index->SetValue(0, 4);
    return index;
  }

  virtual std::shared_ptr<MCMCProposal> CoarseProposal (std::shared_ptr<MultiIndex> const& index,
                                                        std::shared_ptr<AbstractSamplingProblem> const& coarseProblem,
                                                        std::shared_ptr<SingleChainMCMC> const& coarseChain) override {
    pt::ptree pt;
    pt.put("Subsampling", 0);

    return std::make_shared<SubsamplingMIProposal>(pt, coarseProblem, coarseChain);
  }

  virtual std::shared_ptr<AbstractSamplingProblem> SamplingProblem (std::shared_ptr<MultiIndex> const& index) override {
    return std::make_shared<MySamplingProblem>(index);
  }

  virtual std::shared_ptr<MIInterpolation> Interpolation (std::shared_ptr<MultiIndex> const& index) override {
    return std::make_shared<MyInterpolation>();
  }

  virtual Eigen::VectorXd StartingPoint (std::shared_ptr<MultiIndex> const& index) override {
    Eigen::VectorXd mu = Eigen::VectorXd::Zero(121);
    return mu;
  }

private:
  pt::ptree pt;
};

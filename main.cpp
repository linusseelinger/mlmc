//
// main.cpp
//
// @author: Pieterjan Robbe
// @date: 27/04/20
//

#include <iostream>
#include "Sandwich.hpp"
#include <random>

int main(int argc, const char * argv[]) {
    
    Sandwich<double> S(10);
    
    // stochastic simulation
    default_random_engine generator;
    normal_distribution<double> distribution(0, 1);
    VectorXd x(121); // random numbers to use in the simulation
    for (int i=0; i<x.size(); i++) {
        x(i) = distribution(generator);
    }
	cout << "--------------------------------" << endl;
    S.setSpatialVaryingYoungsModulus(x);
    S.computeDisplacement();
   // S.planeStress4();
    //S.writeToFile();

	cout << "--------------------------------" << endl;
	Sandwich<double> S2(20);
	S2.setSpatialVaryingYoungsModulus(x);
	S2.computeDisplacement();
	std::cout << S.getDisplacement()(last) << std::endl;
	std::cout << S2.getDisplacement()(last) << std::endl;
	cout << "--------------------------------" << endl;

	std::cout << S.getDisplacement()(last) << std::endl;
	std::cout << S2.getDisplacement()(last) << std::endl;

    
    // now run $ python plotyoungsmod.py
    //         $ python plotdisp.py
    //         $ python plotstress.py

    return 0;
}

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon, Rectangle
from matplotlib.collections import PatchCollection

with open('displacement.txt', 'r') as f:
    lines = f.readlines()
    Lx = float(lines[0])
    Ly = float(lines[1])
    nelx = int(lines[2])
    nely = int(lines[3])
    u_x = []
    u_y = []
    for i in range(4, len(lines), 2):
        u_x.append(float(lines[i]))
        u_y.append(float(lines[i+1]))
    u_x = 10*np.array(u_x).reshape((nelx + 1, nely + 1))
    u_y = 10*np.array(u_y).reshape((nelx + 1, nely + 1))

ys = np.linspace(Ly, 0, nely+1)
xs = np.linspace(0, Lx, nelx+1) 

plt.figure(figsize=(12, 7))
ax = plt.gca()

innerPatches = []
outerPatches = []
innerPatches.append(Rectangle((0, 0), Lx, Ly))
for i in range(len(xs)-1):
    for j in range(len(ys)-1):
        a = np.array([ xs[i]   + u_x[i,   j  ], ys[j]   + u_y[i,   j  ]])
        b = np.array([ xs[i+1] + u_x[i+1, j  ], ys[j]   + u_y[i+1, j  ]])
        c = np.array([ xs[i+1] + u_x[i+1, j+1], ys[j+1] + u_y[i+1, j+1]])
        d = np.array([ xs[i]   + u_x[i,   j+1], ys[j+1] + u_y[i,   j+1]])
        patch = Polygon(np.vstack((a, b, c, d)))
        if j < nely/5 or j >= 4*nely/5 :
            outerPatches.append(patch)
        else:
            innerPatches.append(patch)

ax.add_collection(PatchCollection(innerPatches, edgecolor=(0,0,0,1), facecolor=(1,1,1,0)))
ax.add_collection(PatchCollection(outerPatches, edgecolor=(0,0,0,1), facecolor=(.7,.7,.7,.5)))

plt.axis('equal')
plt.axis('off')

plt.savefig("displacement.png", dpi=300)

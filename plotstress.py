import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

with open('stress.txt', 'r') as f:
    lines = f.readlines()
    Lx = float(lines[0])
    Ly = float(lines[1])
    nelx = int(lines[2])
    nely = int(lines[3])
    sigma_xx = np.array(lines[4].split()).astype(np.float).reshape((nelx + 1, nely + 1));
    sigma_yy = np.array(lines[5].split()).astype(np.float).reshape((nelx + 1, nely + 1));
    tau_xy = np.array(lines[6].split()).astype(np.float).reshape((nelx + 1, nely + 1));

ys = np.linspace(Ly, 0, nely+1)
xs = np.linspace(0, Lx, nelx+1) 

smax = np.max(sigma_xx)

# sigma_xx
plt.figure(figsize=(12, 7))
ax = plt.gca()
im = ax.imshow(sigma_xx.T, extent=[0, Lx, 0, Ly], interpolation="spline16", vmin=-smax/2, vmax=smax/2)
ax.set(xlim=(0, Lx), ylim=(0, Ly))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="2.5%", pad=0.5)
plt.colorbar(im, cax=cax)
plt.savefig("sigma_xx.png", dpi=300)

# sigma_yy
plt.figure(figsize=(12, 7))
ax = plt.gca()
im = ax.imshow(sigma_yy.T, extent=[0, Lx, 0, Ly], interpolation="spline16", vmin=-smax/2, vmax=smax/2)
ax.set(xlim=(0, Lx), ylim=(0, Ly))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="2.5%", pad=0.5)
plt.colorbar(im, cax=cax)
plt.savefig("sigma_yy.png", dpi=300)

# tau_xy
plt.figure(figsize=(12, 7))
ax = plt.gca()
im = ax.imshow(tau_xy.T, extent=[0, Lx, 0, Ly], interpolation="spline16", vmin=-smax/2, vmax=smax/2)
ax.set(xlim=(0, Lx), ylim=(0, Ly))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="2.5%", pad=0.5)
plt.colorbar(im, cax=cax)
plt.savefig("tau_xy.png", dpi=300)

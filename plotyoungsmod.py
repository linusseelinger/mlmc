import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

with open('youngs_modulus.txt', 'r') as f:
    lines = f.readlines()
    Lx = float(lines[0])
    Ly = float(lines[1])
    nelx = int(lines[2])
    nely = int(lines[3])
    Ed = []
    for i in range(4, len(lines)):
        Ed.append(float(lines[i]))
    Ed = np.array(Ed).reshape((nelx, nely))

ys = np.linspace(Ly-Ly/nely, Ly/nely, nely)
xs = np.linspace(Lx/nelx, Lx-Lx/nelx, nelx) 
for i in range(len(xs)):
    for j in range(len(ys)):
        if j < nely/5 or j >= 4*nely/5 :
            Ed[i, j] = np.log((Ed[i, j] / 200e9 - 1)/0.01)
        else:
            Ed[i, j] = np.log((Ed[i, j] / 30e9 - 1)/0.01)

plt.figure(figsize=(12, 7))
ax = plt.gca()
im = ax.imshow(Ed.T, extent=[0, Lx, 0, Ly], interpolation="spline16")
ax.set(xlim=(0, Lx), ylim=(0, Ly))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="2.5%", pad=0.5)
plt.colorbar(im, cax=cax)
plt.savefig("Youngs_modulus.png", dpi=300)
